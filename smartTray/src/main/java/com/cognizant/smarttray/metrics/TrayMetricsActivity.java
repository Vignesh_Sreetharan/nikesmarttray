package com.cognizant.smarttray.metrics;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.cognizant.smarttray.R;

/**
 * Created by 452781 on 8/24/2016.
 */
public class TrayMetricsActivity extends AppCompatActivity {

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tray_metrics);
    }
}
