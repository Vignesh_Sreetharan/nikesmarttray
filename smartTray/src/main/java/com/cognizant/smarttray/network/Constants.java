package com.cognizant.smarttray.network;

/**
 * Created by 452781 on 9/26/2016.
 */
public class Constants {


    public static final int SPLASH_TIME_OUT = 2000;
    //    public static final String REQUEST_TRAY_DATA_URL = "http://c5bdb9cc606747c59d8ba3c2ec161473.cloudapp.net/api/Device/GetAllTrayData";
    //    public static final String REQUEST_SINGLE_TRAY_DATA_URL = "http://c5bdb9cc606747c59d8ba3c2ec161473.cloudapp.net/api/Device/GetTrayData";
    public static final String REQUEST_TRAY_DATA_URL = "http://rmethapi.azurewebsites.net/api/Device/GetAllTrayData";
    public static final String REQUEST_SINGLE_TRAY_DATA_URL = "http://rmethapi.azurewebsites.net/api/Device/GetTrayData";
    public static String CREATE_PO_URL = "http://rmethapi.azurewebsites.net/api/PurchaseServices/CreatePurchaseOrderAPI";
    public static String REQUEST_TRAY_SINGLE_CONFIGURATION_URL = "http://rmethapi.azurewebsites.net/api/Device/GetTraySingleProduct";
    public static String REQUEST_TRAY_CONFIGURE_PRODUCTS_URL = "http://rmethapi.azurewebsites.net/api/Device/GetTrayAllProduct";
    public static String REQUEST_TRAY_CONFIGURATION_UPDATE_URL = "http://rmethapi.azurewebsites.net/api/Device/UpdateTrayData";
}
