package com.cognizant.smarttray.barcode;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cognizant.smarttray.R;

public class InitialActivity extends AppCompatActivity {
    Button button;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);
        textView=(TextView)findViewById(R.id.textView_initial);
        Bundle scanData= getIntent().getExtras();

        if (scanData!=null){
            String name =scanData.getString("name");
            textView.setText(name);
            textView.setVisibility(View.VISIBLE);
        }
        button=(Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(InitialActivity.this,ScanActivity.class);
                startActivity(i);
            }
        });

    }

}
