package com.cognizant.smarttray.barcode;

/**
 * Created by Guest_User on 19/09/16.
 */
public interface ZBarConstants {

    public static final String SCAN_MODES = "SCAN_MODES";
    public static final String SCAN_RESULT = "SCAN_RESULT";
    public static final String SCAN_RESULT_TYPE = "SCAN_RESULT_TYPE";
    public static final String ERROR_INFO = "ERROR_INFO";
}
