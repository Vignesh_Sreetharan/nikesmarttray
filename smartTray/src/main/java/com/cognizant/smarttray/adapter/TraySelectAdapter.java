package com.cognizant.smarttray.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cognizant.smarttray.R;
import com.cognizant.smarttray.model.TraySelectData;

import java.util.Collections;
import java.util.List;

/**
 * Created by 452781 on 8/11/2016.
 */
public class TraySelectAdapter extends RecyclerView.Adapter<TraySelectAdapter.myViewHolder> {

    private final LayoutInflater inflater;

    private Context context;

    List<TraySelectData> dispdata= Collections.emptyList();

    public TraySelectAdapter(Context context, List<TraySelectData> dispdata){
        inflater= LayoutInflater.from(context);
        this.dispdata=dispdata;
        this.context=context;

    }
    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view=inflater.inflate(R.layout.tray_selection_layout, parent, false);
        myViewHolder holder= new myViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {
        TraySelectData current= dispdata.get(position);
        holder.trayNameView.setText(current.trayName);
        holder.dateTime.setText(current.dateTime);

    }


    @Override
    public int getItemCount() {

        return dispdata.size();
    }

    class myViewHolder extends RecyclerView.ViewHolder{
        TextView trayNameView,dateTime;

        public myViewHolder(View itemView) {
            super(itemView);
            trayNameView= (TextView) itemView.findViewById(R.id.tray_name);
            dateTime=(TextView) itemView.findViewById(R.id.date_time);
        }
    }
}