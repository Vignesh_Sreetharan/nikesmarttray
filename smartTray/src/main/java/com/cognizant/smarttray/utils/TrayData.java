package com.cognizant.smarttray.utils;

import com.cognizant.smarttray.model.SlotModel;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by 452781 on 9/26/2016.
 */
public class TrayData implements Serializable {

    String trayName;
    String trayID;

    HashMap<String, SlotModel> sensorMap = new HashMap<String, SlotModel>();


    public String getTrayName() {
        return trayName;
    }

    public void setTrayName(String trayName) {
        this.trayName = trayName;
    }

    public String getTrayID() {
        return trayID;
    }

    public void setTrayID(String trayID) {
        this.trayID = trayID;


    }


    public HashMap<String, SlotModel> getSensorMap() {
        return sensorMap;
    }

    public void setSensorMap(HashMap<String, SlotModel> sensorMap) {
        this.sensorMap = sensorMap;
    }
}
