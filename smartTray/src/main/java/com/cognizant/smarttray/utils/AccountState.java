package com.cognizant.smarttray.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.cognizant.smarttray.BuildConfig;


/**
 * Account related data
 */
public class AccountState {

    private static final String PREF_NAME = BuildConfig.APPLICATION_ID + "_RetailMate";

    private static final String PREF_OFFLINE_MODE = "offline_mode";


    private final SharedPreferences mPref;
    private static AccountState sInstance;


    private static AccountState getInstance() {
        if (sInstance == null) {
            sInstance = new AccountState();
        }
        return sInstance;
    }

    private AccountState() {
        mPref = GlobalClass.getAppContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }


    public static boolean getOfflineMode() {
        return AccountState.getInstance().getBool(PREF_OFFLINE_MODE);
    }

    public static void setOfflineMode(Boolean value) {
        AccountState.getInstance().setBool(PREF_OFFLINE_MODE, value);
    }

    private void setString(String key, String value) {
        AccountState.getInstance().mPref.edit().putString(key, value).apply();
    }

    private String getString(String key) {
        return AccountState.getInstance().mPref.getString(key, null);
    }


    private void setBool(String key, boolean value) {
        AccountState.getInstance().mPref.edit().putBoolean(key, value).apply();
    }

    private boolean getBool(String key) {
        return AccountState.getInstance().mPref.getBoolean(key, false);
    }

    public static void remove(String key) {
        AccountState.getInstance().mPref.edit().remove(key).apply();
    }

    public static void clear() {
        AccountState.getInstance().mPref.edit().clear().apply();
    }

}
