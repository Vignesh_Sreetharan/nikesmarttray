package com.cognizant.smarttray.utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.PopupWindow;

import com.cognizant.smarttray.R;
import com.cognizant.smarttray.traycustomization.ExpandableListAdapter;

/**
 * Created by 452781 on 9/22/2016.
 */
public class ThresholdSetter {
    Context context;


    Button btnDismiss, btnAccept;
    ExpandableListAdapter expandableListAdapter;

    int thresholdvalue = 0;

    public ThresholdSetter(ExpandableListAdapter expandableListAdapter, Context context) {

        this.context = context;
        this.expandableListAdapter = expandableListAdapter;

    }


    public void getPopUp(View parent, int maxLimit, final String listParentName, final Context context) {

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popUpView = inflater.inflate(R.layout.setting_threshold_w, null, false);
        final PopupWindow popup = new PopupWindow(popUpView, screenWidth,
                WindowManager.LayoutParams.WRAP_CONTENT, true);
        popup.setContentView(popUpView);
        btnDismiss = (Button) popUpView.
                findViewById(R.id.cancelButton);

        btnAccept = (Button) popUpView.findViewById(R.id.okButon);

        popup.showAtLocation(parent, Gravity.CENTER_HORIZONTAL, 10, 10);

        final NumberPicker numberpicker = (NumberPicker) popUpView.findViewById(R.id.weightpicker);

        numberpicker.setMinValue(0);
        numberpicker.setMaxValue(maxLimit);
        numberpicker.setFormatter(formatter);
        numberpicker.setWrapSelectorWheel(true);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thresholdvalue = numberpicker.getValue();
                System.out.println("@@# thresholdvalue" + thresholdvalue);

                ExpandableListAdapter._listDataChild.get(listParentName).get(0).setThreshold(thresholdvalue);
                expandableListAdapter.notifyDataSetChanged();

                popup.dismiss();

            }
        });

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thresholdvalue = 0;
                popup.dismiss();
            }
        });

    }


    NumberPicker.Formatter formatter = new NumberPicker.Formatter() {
        @Override
        public String format(int value) {
//            int temp = value * 10;
            int temp = value * 1;
            return "" + temp;
        }
    };

}

