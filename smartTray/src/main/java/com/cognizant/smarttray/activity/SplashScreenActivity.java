package com.cognizant.smarttray.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.cognizant.smarttray.R;
import com.cognizant.smarttray.network.Constants;
import com.cognizant.smarttray.uservalidation.LoginActivity;



public class SplashScreenActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splashactivity);
        final ImageView imageView2= (ImageView) findViewById(R.id.imageView2);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    finish();

                        Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);

                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(SplashScreenActivity.this,SplashScreenActivity.this.findViewById(android.R.id.content) , "profile");
                    startActivity(i, options.toBundle());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, Constants.SPLASH_TIME_OUT);
    }



}