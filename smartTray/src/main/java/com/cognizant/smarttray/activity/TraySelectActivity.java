package com.cognizant.smarttray.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.cognizant.smarttray.R;
import com.cognizant.smarttray.network.Constants;
import com.cognizant.smarttray.network.GsonRequest;
import com.cognizant.smarttray.network.VolleyHelper;
import com.cognizant.smarttray.uservalidation.LoginActivity;
import com.cognizant.smarttray.adapter.TraySelectAdapter;
import com.cognizant.smarttray.model.Response;
import com.cognizant.smarttray.model.TraySelectData;
import com.cognizant.smarttray.utils.AccountState;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 452781 on 8/10/2016.
 */
public class TraySelectActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String LOG = TraySelectActivity.class.getName();

    private RecyclerView traySelectRecyclerView;
    private RecyclerView.Adapter traySelectAdapter;
    List<TraySelectData> dispdata = new ArrayList<>();//List that is passed to the recyclerview
    ProgressDialog progressDialog;
    Response[] responseObject;


    StringBuilder jsonStringBuilder;//for static version

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        super.onCreate(savedInstanceState);

        setContentView(R.layout.tray_selection);
        traySelectRecyclerView = (RecyclerView) findViewById(R.id.tray_list_view);
        progressDialog = new ProgressDialog(TraySelectActivity.this);

        getData();//API call made, passed to list -> adapter for recycler view


        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setElevation(0);
        }


        //to check connection and display error correspondingly

        //network check START
        if (!haveNetworkConnection()) {
            Log.e("TAG", "NOT CONNECTED");
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(TraySelectActivity.this);
            alertDialog.setTitle("No internet connection")
                    .setMessage("Please check connection and try again")
                    .setCancelable(false)
                    .setIconAttribute(android.R.attr.alertDialogIcon)
                    .setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    Intent Intent = new Intent(TraySelectActivity.this, LoginActivity.class);
                                    startActivity(Intent);
                                    finish();
                                }
                            })
                    .show();


        } else {
            Log.e("TAG", "CONNECTED");
//


//
        }

        //network check END


        //handling click event on recycler view
        traySelectRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), traySelectRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                if (position == 0) {
                    Toast.makeText(getApplicationContext(), "Tray not active, please try another tray", Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(TraySelectActivity.this, SingleTrayActivity.class);

                    intent.putExtra("data", responseObject[position]);
                    intent.putExtra("position", position);
                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    public void getData() {
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");
        progressDialog.show();


        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");

        Log.i(LOG, params.toString());


        if (AccountState.getOfflineMode()) {
            offlineTrayDataRead();
        } else {



/*
API CALL GETTING MADE FOR Dynamic Version
 */

        /*API CALL START 1*/
            GsonRequest<Response[]> getResponse =
                    new GsonRequest<>(Request.Method.POST, Constants.REQUEST_TRAY_DATA_URL, Response[].class,

                            new com.android.volley.Response.Listener<Response[]>() {
                                @Override
                                public void onResponse(Response[] response) {
        /* API CALL END 1*/



        /*Common for both static and dynamic START 1*/
                                    responseObject = response;
                                    for (int i = 0; i < response.length; i++) {
                                        TraySelectData current = new TraySelectData();
                                        current.trayName = response[i].getTrayName();
                                        Log.e("TAG", "*******" + current.trayName);
                                        current.dateTime = response[i].getCaptureTime();   //display date n time from api
        /*Common for both static and dynamic END 1*/





        /*Common for both static and dynamic START 2*/
                                        dispdata.add(current);
                                    }
                                    traySelectAdapter = new TraySelectAdapter(getApplicationContext(), dispdata);
                                    traySelectRecyclerView.setAdapter(traySelectAdapter);
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);


                                    traySelectRecyclerView.setLayoutManager(layoutManager);
                                    progressDialog.dismiss();
                                }

        /*Common for both static and dynamic END 2*/
                            }


    /*
API CALL GETTING MADE FOR Dynamic Version contd
 */
                            , new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            NetworkResponse networkResponse = error.networkResponse;
                            if (networkResponse != null && networkResponse.statusCode != 200) {
                                progressDialog.dismiss();
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(TraySelectActivity.this);
                                alertDialog.setTitle("Server error")
                                        .setMessage("Please try after sometime")
                                        .setCancelable(false)
                                        .setIconAttribute(android.R.attr.alertDialogIcon)
                                        .setPositiveButton(
                                                "OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                        finish();
                                                    }
                                                })
                                        .show();


                            }

                        }
                    });
//

            RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            getResponse.setRetryPolicy(policy);
            VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(getResponse);
        }
    }
       /*
API CALL GETTING MADE FOR Dynamic Version END
 */


    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }


    @Override
    protected void onDestroy() {

        super.onDestroy();
    }


    /*Function of network check START*/

    private boolean haveNetworkConnection() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
//            Connectivity for wifi
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    return true;
//            Connectivity for sim
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    return true;
        }
        return false;
    }

      /*Function of network check END*/

    @Override
    public void onClick(View v) {

    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }


    /*Touch listener to differentiate between gesture and touch  //APP CAN FUNCTION WITHOUT THIS */
    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private TraySelectActivity.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final TraySelectActivity.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildLayoutPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }


    void offlineTrayDataRead() {
                /*
        READING FROM ASSETS for STATIC VERSION
         */

        jsonStringBuilder = new StringBuilder();
        InputStream json = null;
        try {
            json = getAssets().open("response.json");
            BufferedReader in =
                    new BufferedReader(new InputStreamReader(json, "UTF-8"));
            String str;

            while ((str = in.readLine()) != null) {
                jsonStringBuilder.append(str);
            }

            in.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        Response[] response;

        Gson gson = new Gson();

        response = gson.fromJson(jsonStringBuilder.toString(), Response[].class);
        Log.e("TAG", "*****" + jsonStringBuilder.toString());
        Log.e("TAG", "*****" + response[1].getDeviceAgentId());
           /*
        READING FROM ASSETS for STATIC VERSION END
         */


        responseObject = response;
        for (int i = 0; i < response.length; i++) {
            TraySelectData current = new TraySelectData();
            current.trayName = response[i].getTrayName();
            Log.e("TAG", "*******" + current.trayName);
            current.dateTime = response[i].getCaptureTime();

            // for static version - display system date
            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            current.dateTime = currentDateTimeString;
// for static version - display system date end
            dispdata.add(current);
        }


        traySelectAdapter = new TraySelectAdapter(getApplicationContext(), dispdata);
        traySelectRecyclerView.setAdapter(traySelectAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);


        traySelectRecyclerView.setLayoutManager(layoutManager);
        progressDialog.dismiss();

    }


}



