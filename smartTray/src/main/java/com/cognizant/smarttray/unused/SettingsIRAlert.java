package com.cognizant.smarttray.unused;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;

import com.cognizant.smarttray.R;

/**
 * Created by 452781 on 8/4/2016.
 */
public class SettingsIRAlert extends Activity {

    String message = "", notificationType = "";
    Long jobassignid;
    Boolean ishistory;
    int limit;
    Dialog d;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent=getIntent();



        if (getIntent() != null) {
            limit=intent.getIntExtra("limit",0);

            Log.e("TAG", String.valueOf(limit));
        }


        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);
        d = new Dialog(this, R.style.NewDialog);
        LayoutInflater infalInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        View mView = infalInflater.inflate(R.layout.setting_threshold_ir, null);
        Button ok = (Button) mView.findViewById(R.id.okButon);
        Button cancel = (Button) mView.findViewById(R.id.cancelButton);
        NumberPicker ircountpicker = (NumberPicker) mView.findViewById(R.id.irpicker);
        ircountpicker.setMinValue(1);
        ircountpicker.setMaxValue(limit);
        ircountpicker.setWrapSelectorWheel(true);


        //Set a value change listener for NumberPicker
        ircountpicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                //Display the newly selected number from picker
                // tv.setText("Selected Number : " + newVal);

            }
        });




        d.setContentView(mView);

        Window window = d.getWindow();
        window.setLayout(screenWidth, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();

                finish();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();

                finish();
            }
        });
        d.show();
    }


    @Override
    public void onBackPressed() {
        d.dismiss();
        this.finish();
    }


}

