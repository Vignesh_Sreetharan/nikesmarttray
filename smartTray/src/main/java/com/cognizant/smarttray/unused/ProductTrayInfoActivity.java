package com.cognizant.smarttray.unused;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.cognizant.smarttray.R;
import com.cognizant.smarttray.uservalidation.LoginActivity;


public class ProductTrayInfoActivity extends AppCompatActivity {

    private static final String LOG = LoginActivity.class.getName();

    private static final String TAG = ProductTrayInfoActivity.class.getName();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.procuct_tray_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setElevation(0);
        }
    }

    public void showSettings(View v) {
        Intent startIntent = new Intent(this, SettingsAlert.class);
        startIntent.putExtra("limit",21);
        startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startIntent);
    }

    public void showMetrics(View v){
//        Intent intent=new Intent(ProductTrayInfoActivity.this,TrayMetricsActivity.class);
//                startActivity(intent);
    openApp(this,"com.xxmassdeveloper.mpchartexample");
//        Toast.makeText(ProductTrayInfoActivity.this,"Metrics",Toast.LENGTH_LONG).show();
    }



    @Override
    public void onResume() {
        super.onResume();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
                case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }





    @Override
    protected void onPause() {
        super.onPause();

    }


    @Override
    protected void onDestroy() {

        super.onDestroy();
    }


    public boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        Intent i = manager.getLaunchIntentForPackage(packageName);
        if (i == null) {
            Toast.makeText(ProductTrayInfoActivity.this,"Contact Service Provider",Toast.LENGTH_LONG).show();
            return false;
            //throw new PackageManager.NameNotFoundException();
        }
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        context.startActivity(i);
        return true;
    }
}