package com.cognizant.smarttray.traycustomization;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.cognizant.smarttray.R;
import com.cognizant.smarttray.model.IRSensorConfiguration;
import com.cognizant.smarttray.model.Response;
import com.cognizant.smarttray.model.Sensors;
import com.cognizant.smarttray.model.SingleTraySlot;
import com.cognizant.smarttray.model.SlotModel;
import com.cognizant.smarttray.model.UVSensorConfiguration;
import com.cognizant.smarttray.model.UpdateTrayConfiguration;
import com.cognizant.smarttray.model.UpdateTrayResponse;
import com.cognizant.smarttray.model.WTSensorConfiguration;
import com.cognizant.smarttray.network.Constants;
import com.cognizant.smarttray.network.GsonRequest;
import com.cognizant.smarttray.network.VolleyHelper;
import com.cognizant.smarttray.utils.AccountState;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 452781 on 9/21/2016.
 */
public class TrayConfiguration extends AppCompatActivity

{

    Button updateTraySettings;
    ImageView slotOne, slotTwo, slotThree;

    ExpandableListView expListView;

    public static ExpandableListAdapter listAdapter;

    List<String> listDataHeader;
    public static HashMap<String, List<SlotModel>> listDataChild;

    Sensors sensors;

    String TrayID;

    Map<String, String> IRMap = new HashMap<String, String>();
    Map<String, String> UVMap = new HashMap<String, String>();
    Map<String, String> WTMap = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.trayconfiguration);

        TrayID = getIntent().getStringExtra("TrayId");

        sensors = new Sensors();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setElevation(0);
        }

        updateTraySettings = (Button) findViewById(R.id.updateTraySettings);
        slotOne = (ImageView) findViewById(R.id.slotOne);
        slotTwo = (ImageView) findViewById(R.id.slotTwo);
        slotThree = (ImageView) findViewById(R.id.slotThree);

        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.expandableListForSlots);

        if (AccountState.getOfflineMode()) {
            getSlotProductsOFFLINE();
        } else {

            //Get Products that can come over slots
            getSlotProducts();

        }

        // preparing list data
        prepareListData();

        listAdapter = new com.cognizant.smarttray.traycustomization.ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);


        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {

                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        // Listview Group collapsed listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {


            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
//                Toast.makeText(
//                        getApplicationContext(),
//                        listDataHeader.get(groupPosition)
//                                + " : "
//                                + listDataChild.get(
//                                listDataHeader.get(groupPosition)).get(
//                                childPosition), Toast.LENGTH_SHORT)
//                        .show();


                return false;
            }
        });

        slotOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expListView.performItemClick(expListView.getAdapter().getView(1, null, null), 1, expListView.getItemIdAtPosition(1));

            }
        });

        slotTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expListView.performItemClick(expListView.getAdapter().getView(2, null, null), 2, expListView.getItemIdAtPosition(2));

            }
        });
        slotThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expListView.performItemClick(expListView.getAdapter().getView(3, null, null), 3, expListView.getItemIdAtPosition(3));

            }
        });


        updateTraySettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AccountState.getOfflineMode()) {
                    Toast.makeText(TrayConfiguration.this, "Tray Configuration updated successfully\n", Toast.LENGTH_SHORT).show();
                } else {

                    System.out.println("@@## Name " + listDataChild.get("Tray Inputs").get(0).getProductName());
                    System.out.println("@@## TrayID " + TrayID);
                    System.out.println("@@## IRName " + listDataChild.get("Slot 1 Configuration").get(0).getProductName());
                    System.out.println("@@## IRITEMID " + listDataChild.get("Slot 1 Configuration").get(0).getItemId());
                    System.out.println("@@## UVITEMID " + listDataChild.get("Slot 2 Configuration").get(0).getItemId());
                    System.out.println("@@## IRThreshold " + listDataChild.get("Slot 1 Configuration").get(0).getThreshold());


                    UpdateTrayConfiguration updateTrayConfiguration = new UpdateTrayConfiguration();

                    updateTrayConfiguration.setTrayName(listDataChild.get("Tray Inputs").get(0).getProductName());
                    updateTrayConfiguration.setDeviceId(TrayID);
                    updateTrayConfiguration.setTrayLocation("Floor 1, Zone A");

//                updateTrayConfiguration.setHumHighThresh(50.0);
//                updateTrayConfiguration.setHumLowThresh(14.0);
//                updateTrayConfiguration.setTempHighThresh(27.0);
//                updateTrayConfiguration.setTempLowThresh(10.0);

                    ArrayList<IRSensorConfiguration> irlist = new ArrayList<IRSensorConfiguration>();
//                ArrayList<UVSensorConfiguration> uvlist = new ArrayList<UVSensorConfiguration>();
                    ArrayList<WTSensorConfiguration> wtlist = new ArrayList<WTSensorConfiguration>();

                    IRSensorConfiguration irsensor = new IRSensorConfiguration();
                    irsensor.setIRThreshold(listDataChild.get("Slot 1 Configuration").get(0).getThreshold());
                    irsensor.setProductId(listDataChild.get("Slot 1 Configuration").get(0).getItemId());
                    irsensor.setSensorId(sensors.getIRSensor001().get(0).getTraySensorId());
                    irlist.add(irsensor);

                    IRSensorConfiguration irsensor1 = new IRSensorConfiguration();
                    irsensor1.setIRThreshold(listDataChild.get("Slot 1 Configuration").get(0).getThreshold());
                    irsensor1.setProductId(listDataChild.get("Slot 1 Configuration").get(0).getItemId());
                    irsensor1.setSensorId(sensors.getIRSensor002().get(0).getTraySensorId());
                    irlist.add(irsensor1);

                    IRSensorConfiguration irsensor2 = new IRSensorConfiguration();
                    irsensor2.setIRThreshold(listDataChild.get("Slot 1 Configuration").get(0).getThreshold());
                    irsensor2.setProductId(listDataChild.get("Slot 1 Configuration").get(0).getItemId());
                    irsensor2.setSensorId(sensors.getIRSensor003().get(0).getTraySensorId());
                    irlist.add(irsensor2);

                    IRSensorConfiguration irsensor3 = new IRSensorConfiguration();
                    irsensor3.setIRThreshold(listDataChild.get("Slot 1 Configuration").get(0).getThreshold());
                    irsensor3.setProductId(listDataChild.get("Slot 1 Configuration").get(0).getItemId());
                    irsensor3.setSensorId(sensors.getIRSensor004().get(0).getTraySensorId());
                    irlist.add(irsensor3);

                    IRSensorConfiguration irsensor4 = new IRSensorConfiguration();
                    irsensor4.setIRThreshold(listDataChild.get("Slot 1 Configuration").get(0).getThreshold());
                    irsensor4.setProductId(listDataChild.get("Slot 1 Configuration").get(0).getItemId());
                    irsensor4.setSensorId(sensors.getIRSensor005().get(0).getTraySensorId());
                    irlist.add(irsensor4);


                    IRSensorConfiguration irsensor5 = new IRSensorConfiguration();
                    irsensor5.setIRThreshold(listDataChild.get("Slot 1 Configuration").get(0).getThreshold());
                    irsensor5.setProductId(listDataChild.get("Slot 1 Configuration").get(0).getItemId());
                    irsensor5.setSensorId(sensors.getIRSensor006().get(0).getTraySensorId());
                    irlist.add(irsensor5);


                    IRSensorConfiguration irsensor6 = new IRSensorConfiguration();
                    irsensor6.setIRThreshold(listDataChild.get("Slot 1 Configuration").get(0).getThreshold());
                    irsensor6.setProductId(listDataChild.get("Slot 1 Configuration").get(0).getItemId());
                    irsensor6.setSensorId(sensors.getIRSensor007().get(0).getTraySensorId());
                    irlist.add(irsensor6);


                    IRSensorConfiguration irsensor7 = new IRSensorConfiguration();
                    irsensor7.setIRThreshold(listDataChild.get("Slot 1 Configuration").get(0).getThreshold());
                    irsensor7.setProductId(listDataChild.get("Slot 1 Configuration").get(0).getItemId());
                    irsensor7.setSensorId(sensors.getIRSensor008().get(0).getTraySensorId());
                    irlist.add(irsensor7);

                    IRSensorConfiguration irsensor8 = new IRSensorConfiguration();
                    irsensor8.setIRThreshold(listDataChild.get("Slot 1 Configuration").get(0).getThreshold());
                    irsensor8.setProductId(listDataChild.get("Slot 1 Configuration").get(0).getItemId());
                    irsensor8.setSensorId(sensors.getIRSensor009().get(0).getTraySensorId());
                    irlist.add(irsensor8);

                    IRSensorConfiguration irsensor9 = new IRSensorConfiguration();
                    irsensor9.setIRThreshold(listDataChild.get("Slot 1 Configuration").get(0).getThreshold());
                    irsensor9.setProductId(listDataChild.get("Slot 1 Configuration").get(0).getItemId());
                    irsensor9.setSensorId(sensors.getIRSensor010().get(0).getTraySensorId());
                    irlist.add(irsensor9);


//                UVSensorConfiguration uvsensor = new UVSensorConfiguration();
//                uvsensor.setUVThreshold(listDataChild.get("Slot 2 Configuration").get(0).getThreshold());
//                uvsensor.setProductId(listDataChild.get("Slot 2 Configuration").get(0).getItemId());
//                uvsensor.setSensorId(sensors.getUVSensor001().get(0).getTraySensorId());
//                uvsensor.setProductHeight(Double.parseDouble(String.valueOf(sensors.getIRSensor001().get(0).getHeight())));
//                uvsensor.setTotalHeight(30.0);
//                uvlist.add(uvsensor);
//
//                UVSensorConfiguration uvsensor1 = new UVSensorConfiguration();
//                uvsensor1.setUVThreshold(listDataChild.get("Slot 2 Configuration").get(0).getThreshold());
//                uvsensor1.setProductId(listDataChild.get("Slot 2 Configuration").get(0).getItemId());
//                uvsensor1.setSensorId(sensors.getUVSensor002().get(0).getTraySensorId());
//                uvsensor1.setProductHeight(Double.parseDouble(String.valueOf(sensors.getIRSensor002().get(0).getHeight())));
//                uvsensor1.setTotalHeight(30.0);
//                uvlist.add(uvsensor1);


                    WTSensorConfiguration wtsensor = new WTSensorConfiguration();

                    wtsensor.setWtThreshold(Double.parseDouble(String.valueOf(listDataChild.get("Slot 2 Configuration").get(0).getThreshold())));
                    wtsensor.setProductId(listDataChild.get("Slot 2 Configuration").get(0).getItemId());
                    wtsensor.setSensorId(sensors.getWTSensor001().get(0).getTraySensorId());
                    wtsensor.setMaxWt(1000.0);
                    wtlist.add(wtsensor);


                    updateTrayConfiguration.setIRSensor(irlist);
//                updateTrayConfiguration.setUVSensor(uvlist);
                    updateTrayConfiguration.setWtSensors(wtlist);


                    Gson gson = new Gson();


                    String jsonObj = gson.toJson(updateTrayConfiguration);


                    Log.e("TAG", "_____Sensors jsonObj----- " + jsonObj);


                    updateConfiguration(jsonObj);
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    /*
         * Preparing the list data
         */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<SlotModel>>();

        // Adding parent data
        listDataHeader.add("Tray Inputs");
        listDataHeader.add("Slot 1 Configuration");
        listDataHeader.add("Slot 2 Configuration");
//        listDataHeader.add("Slot 3 Configuration");

        // Adding child data
        List<SlotModel> trayInputs = new ArrayList<SlotModel>();

        SlotModel slotModelinput = new SlotModel();
        slotModelinput.setProductName("My special Tray");
        slotModelinput.setThreshold(0);
        trayInputs.add(slotModelinput);


        List<SlotModel> slotOne = new ArrayList<SlotModel>();
        SlotModel slotModelir = new SlotModel();
        slotModelir.setProductName("IR");
        slotModelir.setThreshold(0);
        slotModelir.setQRCodeNameMap(IRMap);
        slotOne.add(slotModelir);

//        List<SlotModel> slotTwo = new ArrayList<SlotModel>();
//        SlotModel slotModeluv = new SlotModel();
//        slotModeluv.setProductName("UV");
//        slotModuv.setQRCodeNameMap(UVMap);
//        slotTwo.add(slotModeluv);eluv.setThreshold(0);
//        slotModel


        List<SlotModel> slotThree = new ArrayList<SlotModel>();
        SlotModel slotModelwt = new SlotModel();
        slotModelwt.setProductName("Weight");
        slotModelwt.setThreshold(0);
        slotModelwt.setQRCodeNameMap(WTMap);
        slotThree.add(slotModelwt);


        listDataChild.put(listDataHeader.get(0), trayInputs); // Header, Child data
        listDataChild.put(listDataHeader.get(1), slotOne);
//        listDataChild.put(listDataHeader.get(2), slotTwo);
        listDataChild.put(listDataHeader.get(2), slotThree);
//        listDataChild.put(listDataHeader.get(3), slotThree);
    }


    public static void notifychange() {
        ((BaseExpandableListAdapter) listAdapter).notifyDataSetChanged();
    }


    void getSlotProducts() {
        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("TrayId", TrayID);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        GsonRequest<Sensors> getResponse =
                new GsonRequest<Sensors>(Request.Method.POST, Constants.REQUEST_TRAY_CONFIGURE_PRODUCTS_URL, Sensors.class, params, jsonObject,

                        new com.android.volley.Response.Listener<Sensors>() {
                            @Override
                            public void onResponse(Sensors response) {

                                sensors = response;
                                try {

                                    Log.e("TAG", "_____Sensors-----" + response.getIRSensor001().get(0).getProductName());

                                    for (int i = 0; i < response.getIRSensor001().size(); i++) {
                                        IRMap.put(response.getIRSensor001().get(i).getItemBarcode(), response.getIRSensor001().get(i).getProductName());
                                        IRMap.put(response.getIRSensor001().get(i).getProductName(), response.getIRSensor001().get(i).getItemId());
                                    }

//                                    for (int j = 0; j < response.getUVSensor001().size(); j++) {
//                                        UVMap.put(response.getUVSensor001().get(j).getItemBarcode(), response.getUVSensor001().get(j).getProductName());
//                                    }

                                    for (int z = 0; z < response.getWTSensor001().size(); z++) {
                                        WTMap.put(response.getWTSensor001().get(z).getItemBarcode(), response.getWTSensor001().get(z).getProductName());
                                        WTMap.put(response.getWTSensor001().get(z).getProductName(), response.getWTSensor001().get(z).getItemId());
                                    }
                                } catch (NullPointerException | IndexOutOfBoundsException e) {
                                    Toast.makeText(TrayConfiguration.this, "Data not available for this tray", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }


                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(TrayConfiguration.this, "Check internet connection", Toast.LENGTH_SHORT).show();


                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getResponse.setRetryPolicy(policy);
        VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(getResponse);

    }


    //API call to update tray data in AZure and AX

    private void updateConfiguration(String jsonString) {


        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
//        params.put("TrayId", "CA3B37E3-E96E-467E-97F6-E5291A66346D");


        //API CALL GETTING MADE FOR Dynamic Version

        GsonRequest<UpdateTrayResponse> getResponse =
                new GsonRequest<UpdateTrayResponse>(Request.Method.POST, Constants.REQUEST_TRAY_CONFIGURATION_UPDATE_URL, UpdateTrayResponse.class, params, jsonObject,

                        new com.android.volley.Response.Listener<UpdateTrayResponse>() {
                            @Override
                            public void onResponse(UpdateTrayResponse response) {
                                Toast.makeText(TrayConfiguration.this, "Your Tray\n" + response.getResponse(), Toast.LENGTH_SHORT).show();

                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(TrayConfiguration.this, "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getResponse.setRetryPolicy(policy);
        VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(getResponse);


    }


    /*
    OFFLine version
     */

    void getSlotProductsOFFLINE() {
        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");

        Gson gson = new Gson();
        sensors = gson.fromJson(loadsingtrayslotconfigResponse(), Sensors.class);

        try {


            for (int i = 0; i < sensors.getIRSensor001().size(); i++) {
                IRMap.put(sensors.getIRSensor001().get(i).getItemBarcode(), sensors.getIRSensor001().get(i).getProductName());
                IRMap.put(sensors.getIRSensor001().get(i).getProductName(), sensors.getIRSensor001().get(i).getItemId());
            }


            for (int z = 0; z < sensors.getWTSensor001().size(); z++) {
                WTMap.put(sensors.getWTSensor001().get(z).getItemBarcode(), sensors.getWTSensor001().get(z).getProductName());
                WTMap.put(sensors.getWTSensor001().get(z).getProductName(), sensors.getWTSensor001().get(z).getItemId());
            }
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            Toast.makeText(TrayConfiguration.this, "Data not available for this tray", Toast.LENGTH_SHORT).show();
        }


    }


    public String loadsingtrayslotconfigResponse() {
        String json = null;
        try {
            InputStream is = getAssets().open("trayslotconfig.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}

