package com.cognizant.smarttray.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 452781 on 10/4/2016.
 */
public class Sensors implements Serializable {
    @SerializedName("WTSensor001")
    @Expose
    private List<SensorItems> wTSensor001 = new ArrayList<SensorItems>();
    @SerializedName("IRSensor001")
    @Expose
    private List<SensorItems> iRSensor001 = new ArrayList<SensorItems>();
    @SerializedName("IRSensor002")
    @Expose
    private List<SensorItems> iRSensor002 = new ArrayList<SensorItems>();
    @SerializedName("IRSensor003")
    @Expose
    private List<SensorItems> iRSensor003 = new ArrayList<SensorItems>();
    @SerializedName("IRSensor004")
    @Expose
    private List<SensorItems> iRSensor004 = new ArrayList<SensorItems>();
    @SerializedName("IRSensor005")
    @Expose
    private List<SensorItems> iRSensor005 = new ArrayList<SensorItems>();
    @SerializedName("IRSensor006")
    @Expose
    private List<SensorItems> iRSensor006 = new ArrayList<SensorItems>();
    @SerializedName("IRSensor007")
    @Expose
    private List<SensorItems> iRSensor007 = new ArrayList<SensorItems>();
    @SerializedName("IRSensor008")
    @Expose
    private List<SensorItems> iRSensor008 = new ArrayList<SensorItems>();
    @SerializedName("IRSensor009")
    @Expose
    private List<SensorItems> iRSensor009 = new ArrayList<SensorItems>();
    @SerializedName("IRSensor010")
    @Expose
    private List<SensorItems> iRSensor010 = new ArrayList<SensorItems>();

    /**
     * @return The wTSensor001
     */
    public List<SensorItems> getWTSensor001() {
        return wTSensor001;
    }

    /**
     * @param wTSensor001 The WTSensor001
     */
    public void setWTSensor001(List<SensorItems> wTSensor001) {
        this.wTSensor001 = wTSensor001;
    }

    /**
     * @return The iRSensor001
     */
    public List<SensorItems> getIRSensor001() {
        return iRSensor001;
    }

    /**
     * @param iRSensor001 The IRSensor001
     */
    public void setIRSensor001(List<SensorItems> iRSensor001) {
        this.iRSensor001 = iRSensor001;
    }

    /**
     * @return The iRSensor002
     */
    public List<SensorItems> getIRSensor002() {
        return iRSensor002;
    }

    /**
     * @param iRSensor002 The IRSensor002
     */
    public void setIRSensor002(List<SensorItems> iRSensor002) {
        this.iRSensor002 = iRSensor002;
    }

    /**
     * @return The iRSensor003
     */
    public List<SensorItems> getIRSensor003() {
        return iRSensor003;
    }

    /**
     * @param iRSensor003 The IRSensor003
     */
    public void setIRSensor003(List<SensorItems> iRSensor003) {
        this.iRSensor003 = iRSensor003;
    }

    /**
     * @return The iRSensor004
     */
    public List<SensorItems> getIRSensor004() {
        return iRSensor004;
    }

    /**
     * @param iRSensor004 The IRSensor004
     */
    public void setIRSensor004(List<SensorItems> iRSensor004) {
        this.iRSensor004 = iRSensor004;
    }

    /**
     * @return The iRSensor005
     */
    public List<SensorItems> getIRSensor005() {
        return iRSensor005;
    }

    /**
     * @param iRSensor005 The IRSensor005
     */
    public void setIRSensor005(List<SensorItems> iRSensor005) {
        this.iRSensor005 = iRSensor005;
    }

    /**
     * @return The iRSensor006
     */
    public List<SensorItems> getIRSensor006() {
        return iRSensor006;
    }

    /**
     * @param iRSensor006 The IRSensor006
     */
    public void setIRSensor006(List<SensorItems> iRSensor006) {
        this.iRSensor006 = iRSensor006;
    }

    /**
     * @return The iRSensor007
     */
    public List<SensorItems> getIRSensor007() {
        return iRSensor007;
    }

    /**
     * @param iRSensor007 The IRSensor007
     */
    public void setIRSensor007(List<SensorItems> iRSensor007) {
        this.iRSensor007 = iRSensor007;
    }

    /**
     * @return The iRSensor008
     */
    public List<SensorItems> getIRSensor008() {
        return iRSensor008;
    }

    /**
     * @param iRSensor008 The IRSensor008
     */
    public void setIRSensor008(List<SensorItems> iRSensor008) {
        this.iRSensor008 = iRSensor008;
    }

    /**
     * @return The iRSensor009
     */
    public List<SensorItems> getIRSensor009() {
        return iRSensor009;
    }

    /**
     * @param iRSensor009 The IRSensor009
     */
    public void setIRSensor009(List<SensorItems> iRSensor009) {
        this.iRSensor009 = iRSensor009;
    }

    /**
     * @return The iRSensor010
     */
    public List<SensorItems> getIRSensor010() {
        return iRSensor010;
    }

    /**
     * @param iRSensor010 The IRSensor010
     */
    public void setIRSensor010(List<SensorItems> iRSensor010) {
        this.iRSensor010 = iRSensor010;
    }

}

