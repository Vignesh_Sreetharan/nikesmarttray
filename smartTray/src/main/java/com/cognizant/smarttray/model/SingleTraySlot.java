package com.cognizant.smarttray.model;


import java.util.ArrayList;
import java.util.List;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class SingleTraySlot {

    @SerializedName("value")
    @Expose
    private List<SlotModel> value = new ArrayList<SlotModel>();

    /**
     * @return The value
     */
    public List<SlotModel> getValue() {
        return value;
    }

    /**
     * @param value The value
     */
    public void setValue(List<SlotModel> value) {
        this.value = value;
    }

}