package com.cognizant.smarttray.model;

import java.io.Serializable;

/**
 * Created by 452781 on 8/17/2016.
 */
public class TrayDetailsData implements Serializable {
    public String title;
    public String inStoreCount;
    public String inStoreTotalCount;
    public String inStoreColour;
    public String warehouseCount;
    public String warehouseTotalCount;
    public String warehouseColour;
    public String poID;
}
