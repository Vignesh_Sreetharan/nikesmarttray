package com.cognizant.smarttray.model;

/**
 * Created by 452781 on 10/6/2016.
 */
public class UpdateTrayResponse {

    String Response;

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        this.Response = response;
    }
}
