package com.cognizant.smarttray.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UpdateTrayConfiguration {

    private String TrayName;

    private String TrayLocation;


    @SerializedName("DeviceId")
    @Expose
    private String deviceId;
    @SerializedName("WtSensors")
    @Expose
    private List<WTSensorConfiguration> wtSensors = new ArrayList<WTSensorConfiguration>();
    @SerializedName("IRSensor")
    @Expose
    private List<IRSensorConfiguration> iRSensor = new ArrayList<IRSensorConfiguration>();
//    @SerializedName("UVSensor")
//    @Expose
//    private List<UVSensorConfiguration> uVSensor = new ArrayList<UVSensorConfiguration>();
//    @SerializedName("TempHighThresh")
//    @Expose
//    private Double tempHighThresh;
//    @SerializedName("TempLowThresh")
//    @Expose
//    private Double tempLowThresh;
//    @SerializedName("HumHighThresh")
//    @Expose
//    private Double humHighThresh;
//    @SerializedName("HumLowThresh")
//    @Expose
//    private Double humLowThresh;
//    @SerializedName("LowThresh")
//    @Expose
//    private Double lowThresh;
//    @SerializedName("HighThresh")
//    @Expose
//    private Double highThresh;


    public String getTrayName() {
        return TrayName;
    }

    public void setTrayName(String trayName) {
        this.TrayName = trayName;
    }

    public String getTrayLocation() {
        return TrayLocation;
    }

    public void setTrayLocation(String trayLocation) {
        TrayLocation = trayLocation;
    }

    /**
     * @return The deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The DeviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return The wtSensors
     */
    public List<WTSensorConfiguration> getWtSensors() {
        return wtSensors;
    }

    /**
     * @param wtSensors The WtSensors
     */
    public void setWtSensors(List<WTSensorConfiguration> wtSensors) {
        this.wtSensors = wtSensors;
    }

    /**
     * @return The iRSensor
     */
    public List<IRSensorConfiguration> getIRSensor() {
        return iRSensor;
    }

    /**
     * @param iRSensor The IRSensor
     */
    public void setIRSensor(List<IRSensorConfiguration> iRSensor) {
        this.iRSensor = iRSensor;
    }

//    /**
//     * @return The uVSensor
//     */
//    public List<UVSensorConfiguration> getUVSensor() {
//        return uVSensor;
//    }
//
//    /**
//     * @param uVSensor The UVSensor
//     */
//    public void setUVSensor(List<UVSensorConfiguration> uVSensor) {
//        this.uVSensor = uVSensor;
//    }
//
//    /**
//     * @return The tempHighThresh
//     */
//    public Double getTempHighThresh() {
//        return tempHighThresh;
//    }
//
//    /**
//     * @param tempHighThresh The TempHighThresh
//     */
//    public void setTempHighThresh(Double tempHighThresh) {
//        this.tempHighThresh = tempHighThresh;
//    }
//
//    /**
//     * @return The tempLowThresh
//     */
//    public Double getTempLowThresh() {
//        return tempLowThresh;
//    }
//
//    /**
//     * @param tempLowThresh The TempLowThresh
//     */
//    public void setTempLowThresh(Double tempLowThresh) {
//        this.tempLowThresh = tempLowThresh;
//    }
//
//    /**
//     * @return The humHighThresh
//     */
//    public Double getHumHighThresh() {
//        return humHighThresh;
//    }
//
//    /**
//     * @param humHighThresh The HumHighThresh
//     */
//    public void setHumHighThresh(Double humHighThresh) {
//        this.humHighThresh = humHighThresh;
//    }
//
//    /**
//     * @return The humLowThresh
//     */
//    public Double getHumLowThresh() {
//        return humLowThresh;
//    }
//
//    /**
//     * @param humLowThresh The HumLowThresh
//     */
//    public void setHumLowThresh(Double humLowThresh) {
//        this.humLowThresh = humLowThresh;
//    }
//
//    /**
//     * @return The lowThresh
//     */
//    public Double getLowThresh() {
//        return lowThresh;
//    }
//
//    /**
//     * @param lowThresh The LowThresh
//     */
//    public void setLowThresh(Double lowThresh) {
//        this.lowThresh = lowThresh;
//    }
//
//    /**
//     * @return The highThresh
//     */
//    public Double getHighThresh() {
//        return highThresh;
//    }
//
//    /**
//     * @param highThresh The HighThresh
//     */
//    public void setHighThresh(Double highThresh) {
//        this.highThresh = highThresh;
//    }

}