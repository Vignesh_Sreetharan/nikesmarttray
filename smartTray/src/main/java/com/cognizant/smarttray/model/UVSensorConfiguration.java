

/**
 * Created by 452781 on 10/4/2016.
 */

package com.cognizant.smarttray.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UVSensorConfiguration {

    @SerializedName("SensorId")
    @Expose
    private String sensorId;
    @SerializedName("ProductId")
    @Expose
    private String productId;
    @SerializedName("TotalHeight")
    @Expose
    private Double totalHeight;
    @SerializedName("ProductHeight")
    @Expose
    private Double productHeight;
    @SerializedName("UVThreshold")
    @Expose
    private Integer uVThreshold;

    /**
     * @return The sensorId
     */
    public String getSensorId() {
        return sensorId;
    }

    /**
     * @param sensorId The SensorId
     */
    public void setSensorId(String sensorId) {
        this.sensorId = sensorId;
    }

    /**
     * @return The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId The ProductId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return The totalHeight
     */
    public Double getTotalHeight() {
        return totalHeight;
    }

    /**
     * @param totalHeight The TotalHeight
     */
    public void setTotalHeight(Double totalHeight) {
        this.totalHeight = totalHeight;
    }

    /**
     * @return The productHeight
     */
    public Double getProductHeight() {
        return productHeight;
    }

    /**
     * @param productHeight The ProductHeight
     */
    public void setProductHeight(Double productHeight) {
        this.productHeight = productHeight;
    }

    /**
     * @return The uVThreshold
     */
    public Integer getUVThreshold() {
        return uVThreshold;
    }

    /**
     * @param uVThreshold The UVThreshold
     */
    public void setUVThreshold(Integer uVThreshold) {
        this.uVThreshold = uVThreshold;
    }

}
