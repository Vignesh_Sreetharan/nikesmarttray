package com.cognizant.smarttray.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 452781 on 8/10/2016.
 */
public class Response implements Serializable {


    @SerializedName("StoreName")
    @Expose
    private String storeName;
    @SerializedName("TrayName")
    @Expose
    private String trayName;
    @SerializedName("DeviceAgentId")
    @Expose
    private String deviceAgentId;
    @SerializedName("DeviceLocation")
    @Expose
    private String deviceLocation;
    @SerializedName("CaptureTime")
    @Expose
    private String captureTime;
    @SerializedName("Comment")
    @Expose
    private String comment;
    @SerializedName("IRSensor009")
    @Expose
    private Integer iRSensor009;
    @SerializedName("WTSensor001")
    @Expose
    private Integer wTSensor001;
    @SerializedName("IRSensor008")
    @Expose
    private Integer iRSensor008;
    @SerializedName("IRSensor001")
    @Expose
    private Integer iRSensor001;
    @SerializedName("IRSensor005")
    @Expose
    private Integer iRSensor005;
    @SerializedName("IRSensor007")
    @Expose
    private Integer iRSensor007;
    @SerializedName("IRSensor002")
    @Expose
    private Integer iRSensor002;
    @SerializedName("IRSensor003")
    @Expose
    private Integer iRSensor003;
    @SerializedName("IRSensor010")
    @Expose
    private Integer iRSensor010;
    @SerializedName("IRSensor006")
    @Expose
    private Integer iRSensor006;
    @SerializedName("IRSensor004")
    @Expose
    private Integer iRSensor004;

    /**
     *
     * @return
     * The storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     *
     * @param storeName
     * The StoreName
     */
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    /**
     *
     * @return
     * The trayName
     */
    public String getTrayName() {
        return trayName;
    }

    /**
     *
     * @param trayName
     * The TrayName
     */
    public void setTrayName(String trayName) {
        this.trayName = trayName;
    }

    /**
     *
     * @return
     * The deviceAgentId
     */
    public String getDeviceAgentId() {
        return deviceAgentId;
    }

    /**
     *
     * @param deviceAgentId
     * The DeviceAgentId
     */
    public void setDeviceAgentId(String deviceAgentId) {
        this.deviceAgentId = deviceAgentId;
    }

    /**
     *
     * @return
     * The deviceLocation
     */
    public String getDeviceLocation() {
        return deviceLocation;
    }

    /**
     *
     * @param deviceLocation
     * The DeviceLocation
     */
    public void setDeviceLocation(String deviceLocation) {
        this.deviceLocation = deviceLocation;
    }

    /**
     *
     * @return
     * The captureTime
     */
    public String getCaptureTime() {
        return captureTime;
    }

    /**
     *
     * @param captureTime
     * The CaptureTime
     */
    public void setCaptureTime(String captureTime) {
        this.captureTime = captureTime;
    }

    /**
     *
     * @return
     * The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     * The Comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     *
     * @return
     * The iRSensor009
     */
    public Integer getIRSensor009() {
        return iRSensor009;
    }

    /**
     *
     * @param iRSensor009
     * The IRSensor009
     */
    public void setIRSensor009(Integer iRSensor009) {
        this.iRSensor009 = iRSensor009;
    }

    /**
     *
     * @return
     * The wTSensor001
     */
    public Integer getWTSensor001() {
        return wTSensor001;
    }

    /**
     *
     * @param wTSensor001
     * The WTSensor001
     */
    public void setWTSensor001(Integer wTSensor001) {
        this.wTSensor001 = wTSensor001;
    }

    /**
     *
     * @return
     * The iRSensor008
     */
    public Integer getIRSensor008() {
        return iRSensor008;
    }

    /**
     *
     * @param iRSensor008
     * The IRSensor008
     */
    public void setIRSensor008(Integer iRSensor008) {
        this.iRSensor008 = iRSensor008;
    }

    /**
     *
     * @return
     * The iRSensor001
     */
    public Integer getIRSensor001() {
        return iRSensor001;
    }

    /**
     *
     * @param iRSensor001
     * The IRSensor001
     */
    public void setIRSensor001(Integer iRSensor001) {
        this.iRSensor001 = iRSensor001;
    }

    /**
     *
     * @return
     * The iRSensor005
     */
    public Integer getIRSensor005() {
        return iRSensor005;
    }

    /**
     *
     * @param iRSensor005
     * The IRSensor005
     */
    public void setIRSensor005(Integer iRSensor005) {
        this.iRSensor005 = iRSensor005;
    }

    /**
     *
     * @return
     * The iRSensor007
     */
    public Integer getIRSensor007() {
        return iRSensor007;
    }

    /**
     *
     * @param iRSensor007
     * The IRSensor007
     */
    public void setIRSensor007(Integer iRSensor007) {
        this.iRSensor007 = iRSensor007;
    }

    /**
     *
     * @return
     * The iRSensor002
     */
    public Integer getIRSensor002() {
        return iRSensor002;
    }

    /**
     *
     * @param iRSensor002
     * The IRSensor002
     */
    public void setIRSensor002(Integer iRSensor002) {
        this.iRSensor002 = iRSensor002;
    }

    /**
     *
     * @return
     * The iRSensor003
     */
    public Integer getIRSensor003() {
        return iRSensor003;
    }

    /**
     *
     * @param iRSensor003
     * The IRSensor003
     */
    public void setIRSensor003(Integer iRSensor003) {
        this.iRSensor003 = iRSensor003;
    }

    /**
     *
     * @return
     * The iRSensor010
     */
    public Integer getIRSensor010() {
        return iRSensor010;
    }

    /**
     *
     * @param iRSensor010
     * The IRSensor010
     */
    public void setIRSensor010(Integer iRSensor010) {
        this.iRSensor010 = iRSensor010;
    }

    /**
     *
     * @return
     * The iRSensor006
     */
    public Integer getIRSensor006() {
        return iRSensor006;
    }

    /**
     *
     * @param iRSensor006
     * The IRSensor006
     */
    public void setIRSensor006(Integer iRSensor006) {
        this.iRSensor006 = iRSensor006;
    }

    /**
     *
     * @return
     * The iRSensor004
     */
    public Integer getIRSensor004() {
        return iRSensor004;
    }

    /**
     *
     * @param iRSensor004
     * The IRSensor004
     */
    public void setIRSensor004(Integer iRSensor004) {
        this.iRSensor004 = iRSensor004;
    }
}
