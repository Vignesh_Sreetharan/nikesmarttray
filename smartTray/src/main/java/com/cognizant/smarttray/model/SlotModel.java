package com.cognizant.smarttray.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 452781 on 9/23/2016.
 */
public class SlotModel implements Serializable {

/*
    {
        "SensorDeviceType": "IRSensor",
            "SensorName": "IRSensor004",
            "TraySensorId": "3FEC45B1-261C-4E5F-B296-EFE4EA7FC804",
            "ItemId": "6001",
            "ProductName": "Dailies Contact Lenses",
            "Height": 2,
            "Weight": 6
    }*/

    //    String productID;
//    String productName;
    int threshold;
    Map<String, String> QRCodeNameMap = new HashMap<String, String>();
//    float weight, height;
//    String sensorID;
//    String sensorType;
//    String SensorName;
//
//
//    public String getSensorType() {
//        return sensorType;
//    }
//
//    public void setSensorType(String sensorType) {
//        this.sensorType = sensorType;
//    }
//
//    public String getSensorName() {
//        return SensorName;
//    }
//
//    public void setSensorName(String sensorName) {
//        SensorName = sensorName;
//    }
//
//    public String getProductName() {
//        return productName;
//    }
//
//    public void setProductName(String productName) {
//        this.productName = productName;
//    }
//


    public Map<String, String> getQRCodeNameMap() {
        return QRCodeNameMap;
    }

    public void setQRCodeNameMap(Map<String, String> QRCodeNameMap) {
        this.QRCodeNameMap = QRCodeNameMap;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }
//
//    public float getWeight() {
//        return weight;
//    }
//
//    public void setWeight(float weight) {
//        this.weight = weight;
//    }
//
//    public float getHeight() {
//        return height;
//    }
//
//    public void setHeight(float height) {
//        this.height = height;
//    }
//
//    public String getProductID() {
//        return productID;
//    }
//
//    public void setProductID(String productID) {
//        this.productID = productID;
//    }
//
//    public String getSensorID() {
//        return sensorID;
//    }
//
//    public void setSensorID(String sensorID) {
//        this.sensorID = sensorID;
//    }

    @SerializedName("SensorDeviceType")
    @Expose
    private String sensorDeviceType;
    @SerializedName("SensorName")
    @Expose
    private String sensorName;
    @SerializedName("TraySensorId")
    @Expose
    private String traySensorId;
    @SerializedName("ItemId")
    @Expose
    private String itemId;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("Height")
    @Expose
    private Integer height;
    @SerializedName("Weight")
    @Expose
    private Integer weight;

    /**
     * @return The sensorDeviceType
     */
    public String getSensorDeviceType() {
        return sensorDeviceType;
    }

    /**
     * @param sensorDeviceType The SensorDeviceType
     */
    public void setSensorDeviceType(String sensorDeviceType) {
        this.sensorDeviceType = sensorDeviceType;
    }

    /**
     * @return The sensorName
     */
    public String getSensorName() {
        return sensorName;
    }

    /**
     * @param sensorName The SensorName
     */
    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }

    /**
     * @return The traySensorId
     */
    public String getTraySensorId() {
        return traySensorId;
    }

    /**
     * @param traySensorId The TraySensorId
     */
    public void setTraySensorId(String traySensorId) {
        this.traySensorId = traySensorId;
    }

    /**
     * @return The itemId
     */
    public String getItemId() {
        return itemId;
    }

    /**
     * @param itemId The ItemId
     */
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    /**
     * @return The productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName The ProductName
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return The height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * @param height The Height
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * @return The weight
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * @param weight The Weight
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }


}
