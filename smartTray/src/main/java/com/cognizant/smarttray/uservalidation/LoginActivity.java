package com.cognizant.smarttray.uservalidation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;

import com.cognizant.smarttray.R;
import com.cognizant.smarttray.activity.TraySelectActivity;
import com.cognizant.smarttray.utils.AccountState;


public class LoginActivity extends AppCompatActivity {
    private static final String LOG = LoginActivity.class.getName();
    private LoginActivity loginActivity;
    private EditText mUsername, mPassword;

    private int screenWidth;
    String userName;
    String passWord;


    View offlineview;
    Switch offlineswitch;

    ImageView imageView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        loginActivity = this;
        setContentView(R.layout.activity_login);

        imageView2 = (ImageView) findViewById(R.id.imageView2);
        offlineswitch = (Switch) findViewById(R.id.offlineswitch);

        offlineswitch.setChecked(AccountState.getOfflineMode());

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (offlineswitch.getVisibility() == View.INVISIBLE) {
                    offlineswitch.setVisibility(View.VISIBLE);
                } else {
                    offlineswitch.setVisibility(View.INVISIBLE);
                }
            }
        });

        offlineswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    offlineswitch.setChecked(true);
                    AccountState.setOfflineMode(true);
                } else {
                    offlineswitch.setChecked(false);
                    AccountState.setOfflineMode(false);

                }
            }
        });


        DisplayMetrics metrics = getResources().getDisplayMetrics();
        screenWidth = (int) (metrics.widthPixels * 0.80);
        mUsername = (EditText) findViewById(R.id.txt_username);
        mPassword = (EditText) findViewById(R.id.txt_password);
        Button loginBtn = (Button) findViewById(R.id.submit);


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userName = mUsername.getText().toString().trim();
                passWord = mPassword.getText().toString().trim();

                //Login Validation

                startActivity();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    private void startActivity() {
        Intent startIntent = new Intent(loginActivity, TraySelectActivity.class);
        startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startIntent);
    }

}